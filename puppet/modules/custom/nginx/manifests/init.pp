class nginx {
  package { 'nginx':
    ensure => 'present',
    require => Exec['apt-get update'],
  }

  # Hosts
  file { 'nginx-default-host':
    path => '/etc/nginx/sites-available/default',
    ensure => file,
    replace => true,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/default',
  }

  # Banjo host
  file { 'nginx-banjo-host':
    path => '/etc/nginx/sites-available/banjo.local',
    ensure => file,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/banjo.local',
  }
  #file { 'nginx-banjo-host-enabled':
  #  path => '/etc/nginx/sites-enabled/banjo.local',
  #  target => '/etc/nginx/sites-available/banjo.local',
  #  ensure => link,
  #  #notify => Service['nginx'],
  #  require => File['nginx-banjo-host'],
  #}
  
  # Musicconf host
  file { 'nginx-musicconf-host':
    path => '/etc/nginx/sites-available/musicconf.local',
    ensure => file,
    require => Package['nginx'],
    source => 'puppet:///modules/nginx/musicconf.local',
  }
  file { 'nginx-musicconf-host-enabled':
    path => '/etc/nginx/sites-enabled/musicconf.local',
    target => '/etc/nginx/sites-available/musicconf.local',
    ensure => link,
    #notify => Service['nginx'],
    require => File['nginx-musicconf-host'],
  }

  file { 'nginx-musicconf-cert':
    path => '/etc/nginx/ssl',
    ensure => 'directory',
    recurse => 'remote',
    source => 'puppet:///modules/nginx/ssl',
    require => Package['nginx'],
  }

  service { 'nginx':
    ensure => running,
    require => Package['nginx'],
  }
}