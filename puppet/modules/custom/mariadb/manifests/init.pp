class mariadb {
  #$lockfile = 'config_setup.lock'
  #$password = 'root'

  class {'::mysql::server':
    package_name     => 'mariadb-server',
    restart          => true,
    service_name     => 'mysql',
    root_password    => 'root',
    override_options => {
      mysqld => {
        bind-address => '0.0.0.0',
        character-set-server => 'utf8mb4',
        collation-server     => 'utf8mb4_unicode_ci',
      },
    }
  }

  class {'::mysql::client':
    package_name    => 'mariadb-client',
  }

  service { 'mariadb':
    ensure  => running,
    #require => Package['mariadb-server'],
  }
  
  mysql::db { '*':
    user     => 'root',
    password => 'root',
    host     => '%',
    grant    => ['ALL'],
  }

  #exec { 'sql':
  #  command => "sudo mysql -uroot -D mysql -e \"$sql\"",
  #  require => Service['mariadb'],
  #  creates => "/home/$username/$lockfile",
  #  path => ['/usr/bin', '/usr/sbin',],
  #}
  
  #file { "/home/$username/$lockfile":
  #  ensure  => file,
  #  content => '',
  #  require => Exec['sql'],
  #  mode    => '0644',
  #  owner   => $username,
  #  group   => $username,
  #}
}
