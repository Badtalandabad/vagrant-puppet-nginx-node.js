class php {
  $enhancers = [ 
    'php7.3',
    'php7.3-cli',
    'php7.3-curl',
    'php7.3-dev',
    'php7.3-gmp',
    'php7.3-gd',
    'php7.3-intl',
    'php7.3-json',
    'php7.3-mbstring',
    'php7.3-mysql',
    'php7.3-pgsql',
    'php7.3-readline',
    'php7.3-soap',
    'php7.3-sqlite3',
    'php7.3-xml',
    'php7.3-zip',

    'php7.3-fpm',
  ]
  package { $enhancers:
    ensure  => 'installed',
    require => Exec['apt-get update'],
  }

  # Make sure php7.3-fpm is running
  service { 'php7.3-fpm':
    ensure => running,
    require => Package['php7.3'],
  }

  package { 'php7.3-xdebug':
    ensure  => 'latest',
    require => Package['php7.3'],
  }

  augeas { "php.ini":
    notify  => Service[nginx],
    require => [Package["php7.3"], Package["augeas-tools"]],
    context => "/files/etc/php/7.3/fpm/php.ini",
    #lens => 'Puppet.lns',
    #incl => "/files/etc/php/7.3/fpm/php.ini",
    changes => [
      "set PHP/post_max_size 100M",
      "set PHP/upload_max_filesize 100M",
      "set Date/date.timezone Europe/Samara",
      "set xdebug/zend_extension /usr/lib/php/20180731/xdebug.so",
      "set xdebug/xdebug.remote_enable 1",
      "set xdebug/xdebug.remote_connect_back 1",
      "set xdebug/xdebug.var_display_max_depth -1",
      "set xdebug/xdebug.var_display_max_children -1",
      "set xdebug/xdebug.var_display_max_data -1",
    ];
  }

  package { "curl":
    ensure => installed,
  }
 
  exec { 'install composer':
    command => '/usr/bin/curl -sS https://getcomposer.org/installer | php && sudo mv composer.phar /usr/local/bin/composer',
    require => [Package['curl'], Package['php7.3']],
    cwd    => '/vagrant',
    creates => "/usr/local/bin/composer",
    environment => ["HOME=/home/vagrant"],
  }
}