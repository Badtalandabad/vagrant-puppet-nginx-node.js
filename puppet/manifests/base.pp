package { 'apache2':
  ensure => 'purged',
}

exec { 'apt-get update':
  path => '/usr/bin',
}

package { 'ifupdown':
  ensure => installed
}

package { 'vim':
  ensure => present,
}
file_line { 'vim-set-line':
  require => Package["vim"],
  path => '/etc/vim/vimrc',
  line => 'set number',
}

# ini files editor
package { ["augeas-tools", "libaugeas-dev"]:   
  ensure => installed 
}

file { "/home/$username/.vimrc":
  ensure  => present,
  content => 'set number',
  replace => 'no',
  mode    => '0644',
  owner   => $username,
  group   => $username,
}

file { "/var/www/html/index.html":
  ensure => absent,
}
file { "/var/www/html":
  ensure  => directory,

}
file { "/var/www/html/index.php":
  ensure  => present,
  content => '<?php phpinfo();',
  replace => 'no',
}

exec { 'sudo timedatectl set-timezone Europe/Samara':
  path => ['/usr/bin', '/usr/sbin',],
}

#rsa-key-20170607

ssh_authorized_key { 'ubuntu':
  ensure => present,
  user   => 'ubuntu',
  type   => 'ssh-rsa',
  key    => 'AAAAB3NzaC1yc2EAAAABJQAAAQEAlJPbnKmPkFkDyayWbZQDnksEd0DTQbvNHfEbLNXO0EUUKZ/2e1ojHiTA/urkO7DMtcDiPJXjxJGhVm6Ntg6/Azp777OZw3xAuoZBb3tXn2s1z8n7KQulbWuv56I6fcTYY1j874XKVyKH2kYoA0zWIsWOEsbdiDoopHsTDG/Z0CVOXwZS4ChzIuMGMgk5+7lTFlv6AaTddPx9CMzGHqYUS+AwWhB2HSt50Vgt3UGFbQwJYxNSZxt37O//GwhTidVH3PFqJk5FCGT8VWwnvqemr7e1Jz2RnXaPmV/26DsPNdAEjmbfGhuiGxkcVE21Cx+WJBj6qnhVhJIv/tJReCovQQ==',
}

host { 'localhost':
  ip => '127.0.0.1',
  host_aliases => [
    'banjo.local',
    'musicconf.local',
    'ror.local'
  ],
}



include nginx 
include php
include mariadb
include nodejspack

